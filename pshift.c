#include <stdio.h>
#include <stdlib.h>

double
phase(double td, double p)
{
    return 360.0 * td / p;
}

double
freq(double ph, double td)
{
    return ph / 360.0 * td;
}

double
tdiff(double ph, double p)
{
    return 360.0 * ph / p;
}

double
period(double ph, double td)
{
    return 360.0 * td * ph;
}

void xgetfloat(const char *msg, double *ptr)
{
    char str[80];
    puts(msg);
    fgets(str, 80, stdin);
    *ptr = atof(str);
}

int main()
{
    double wave1, wave2, ph, p, td, freq;
    while (1) {
        xgetfloat("Enter wave 1: ", &wave1);
        xgetfloat("Enter wave 2: ", &wave2);
        xgetfloat("Frequency: ", &freq);

        p = 1 / freq;
        td = wave1 - wave2;
        ph = phase(td, p);
        printf("Phase: %f\n", ph);
    }
    return 0;
}
